package edu.westga.vmemulator.controller;

import edu.westga.vmemulator.model.Segment;

public class CodeGen {

	public static int skipCounter = 0;
	
	private static String popString = 
			"@R13\nM=D\n@SP\nAM=M-1\n"
			+ "D=M\n@R13\nA=M\nM=D\n";

	public static String neg() {
		StringBuilder sb = new StringBuilder();

		sb.append("// neg command\n");
		sb.append("@SP\n");
		sb.append("A=M-1\n");
		sb.append("M=-M\n");

		return sb.toString();
	}

	public static String not() {
		StringBuilder sb = new StringBuilder();

		sb.append("// not command\n");
		sb.append("@SP\n");
		sb.append("A=M-1\n");
		sb.append("M=!M\n");

		return sb.toString();
	}

	public static String sub() {
		StringBuilder sb = new StringBuilder();

		sb.append("// sub command\n");
		sb.append("@SP\n");
		sb.append("AM=M-1\n");
		sb.append("D=M\n");
		sb.append("A=A-1\n");
		sb.append("M=M-D\n");
		return sb.toString();
	}

	public static String eq() {
		StringBuilder sb = new StringBuilder();

		String label = "SKIP_" + skipCounter;
		skipCounter++;
		sb.append("// eq command\n");
		sb.append("@SP\n");
		sb.append("AM=M-1\n");
		sb.append("D=M\n");
		sb.append("A=A-1\n");
		sb.append("D=D-M\n");
		sb.append("@" + label + "\n");
		sb.append("D;JEQ\n");
		sb.append("D=-1\n");
		sb.append("(" + label + ")\n");
		sb.append("D=!D\n");
		sb.append("@SP\n");
		sb.append("A=M-1\n");
		sb.append("M=D\n");
		return sb.toString();
	}

	public static String gt() {
		StringBuilder sb = new StringBuilder();

		String label1 = "SKIP_" + skipCounter;
		skipCounter++;
		String label2 = "SKIP_" + skipCounter;
		skipCounter++;
		sb.append("// gt command\n");
		sb.append("@SP\n");
		sb.append("AM=M-1\n");
		sb.append("D=M\n");
		sb.append("A=A-1\n");
		sb.append("D=M-D\n");
		sb.append("@" + label1 + "\n");
		sb.append("D;JLE\n");
		sb.append("D=-1\n");
		sb.append("@" + label2 + "\n");
		sb.append("0;JMP\n");
		sb.append("(" + label1 + ")\n");
		sb.append("D=0\n");
		sb.append("(" + label2 + ")\n");
		sb.append("@SP\n");
		sb.append("A=M-1\n");
		sb.append("M=D\n");
		return sb.toString();
	}

	public static String lt() {

		StringBuilder sb = new StringBuilder();

		String label1 = "SKIP_" + skipCounter;
		skipCounter++;
		String label2 = "SKIP_" + skipCounter;
		skipCounter++;
		sb.append("// lt command\n");
		sb.append("@SP\n");
		sb.append("AM=M-1\n");
		sb.append("D=M\n");
		sb.append("A=A-1\n");
		sb.append("D=M-D\n");
		sb.append("@" + label1 + "\n");
		sb.append("D;JGE\n");
		sb.append("D=-1\n");
		sb.append("@" + label2 + "\n");
		sb.append("0;JMP\n");
		sb.append("(" + label1 + ")\n");
		sb.append("D=0\n");
		sb.append("(" + label2 + ")\n");
		sb.append("@SP\n");
		sb.append("A=M-1\n");
		sb.append("M=D\n");
		return sb.toString();
	}

	public static String add() {
		StringBuilder sb = new StringBuilder();

		sb.append("// add command\n");
		sb.append("@SP\n");
		sb.append("AM=M-1\n");
		sb.append("D=M\n");
		sb.append("A=A-1\n");
		sb.append("M=D+M\n");
		return sb.toString();
	}

	public static String or() {
		StringBuilder sb = new StringBuilder();

		sb.append("// or command\n");
		sb.append("@SP\n");
		sb.append("AM=M-1\n");
		sb.append("D=M\n");
		sb.append("A=A-1\n");
		sb.append("M=D|M\n");
		return sb.toString();
	}

	public static String and() {
		StringBuilder sb = new StringBuilder();

		sb.append("// and command\n");
		sb.append("@SP\n");
		sb.append("AM=M-1\n");
		sb.append("D=M\n");
		sb.append("A=A-1\n");
		sb.append("M=D&M\n");
		return sb.toString();
	}

	public static String static_push(String name, int offset) {
		StringBuilder sb = new StringBuilder();
		sb.append("// push static " + offset + "  (" + name + ")\n");

		sb.append("@" + name + "." + offset + "\n");
		sb.append("D=M\n");
		sb.append("@SP\n");
		sb.append("A=M\n");
		sb.append("M=D\n");
		sb.append("@SP\n");
		sb.append("M=M+1\n");

		return sb.toString();

	}

	public static String push(Segment segment, int offset) {

		StringBuilder sb = new StringBuilder();
		sb.append("// push " + segment + " " + offset + "\n");
		int value = offset;
		switch (segment) {
		
		case CONSTANT:
			sb.append("@" + value + "\n" );
			sb.append("D=A\n");
			sb.append("@SP\n");
			sb.append("A=M\n");
			sb.append("M=D\n");
			sb.append("@SP\n");
			sb.append("M=M+1\n");
			break;
		case LOCAL:
			sb.append("@" + offset + "\n");
			sb.append("D=A\n");
			sb.append("@LCL\n");
			sb.append("A=M\n");
			sb.append("A=D+A\n");
			sb.append("D=M\n");
			sb.append("@SP\n");
			sb.append("A=M\n");
			sb.append("M=D\n");
			sb.append("@SP\n");
			sb.append("M=M+1\n");
			break;
		case THIS://TODO if this doesn't work need ram address of this
			sb.append("@" + offset + "\n");
			sb.append("D=A\n");
			sb.append("@THIS\n");
			sb.append("A=M\n");
			sb.append("A=D+A\n");
			sb.append("D=M\n");
			sb.append("@SP\n");
			sb.append("A=M\n");
			sb.append("M=D\n");
			sb.append("@SP\n");
			sb.append("M=M+1\n");
			break;
		case THAT:
			sb.append("@" + offset + "\n");
			sb.append("D=A\n");
			sb.append("@THAT\n");
			sb.append("A=M\n");
			sb.append("A=D+A\n");
			sb.append("D=M\n");
			sb.append("@SP\n");
			sb.append("A=M\n");
			sb.append("M=D\n");
			sb.append("@SP\n");
			sb.append("M=M+1\n");
			break;
		case ARGUMENT:
			sb.append("@" + offset + "\n");
			sb.append("D=A\n");
			sb.append("@ARG\n");
			sb.append("A=M\n");
			sb.append("A=D+A\n");
			sb.append("D=M\n");
			sb.append("@SP\n");
			sb.append("A=M\n");
			sb.append("M=D\n");
			sb.append("@SP\n");
			sb.append("M=M+1\n");
			break;
		case TEMP:
			value += 2;
		case POINTER:
			value += 3;
			sb.append("@" + value+ "\n");
			sb.append("D=M\n");
			sb.append("@SP\n");
			sb.append("A=M\n");
			sb.append("M=D\n");
			sb.append("@SP\n");
			sb.append("M=M+1\n");
			break;
		default:
			throw new IllegalArgumentException("Invalid segment " + segment.toString());
		}
		
		return sb.toString();
	}
	
	public static String pop(Segment segment, int offset){
		StringBuilder sb = new StringBuilder();
		sb.append("// Pop " + segment.toString() + " " + offset+"\n");
		int value = offset;

		switch (segment) {
		case THIS:
			sb.append("@0\n");
			sb.append("D=A\n");
			sb.append("@THIS\n");
			sb.append("D=D+M\n");
			sb.append("@" + value + "\n");
			sb.append("D=D+A\n");
			sb.append(popString);
			break;

		case THAT:
			sb.append("@0\n");
			sb.append("D=A\n");
			sb.append("@THAT\n");
			sb.append("D=D+M\n");
			sb.append("@" + value + "\n");
			sb.append("D=D+A\n");
			sb.append(popString);

			break;

		case LOCAL:
			sb.append("@0\n");
			sb.append("D=A\n");
			sb.append("@LCL\n");
			sb.append("D=D+M\n");
			sb.append("@" + value + "\n");
			sb.append("D=D+A\n");
			sb.append(popString);

			break;

		case ARGUMENT:
			sb.append("@ARG\n");
			sb.append("D=M\n");
			sb.append("@" + value + "\n");
			sb.append("D=D+A\n");
			sb.append(popString);
			break;
		case TEMP:
			value += 2;
		case POINTER:
			value += 3;
			sb.append("@" + value + "\n");
			sb.append("D=A\n");
			sb.append(popString);

			break;
		default:
			break;
		}
		
		return sb.toString();
	}
	
	public static String static_pop(String name, int offset) {
		StringBuilder sb = new StringBuilder();
		sb.append("// pop static " + offset + "  (" + name + ")\n");

		sb.append("@SP\n");
		sb.append("AM=M-1\n");
		sb.append("D=M\n");
		sb.append("@" + name + "." +offset + "\n");
		sb.append("M=D\n");

		return sb.toString();

	}
}
