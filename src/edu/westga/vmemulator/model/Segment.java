package edu.westga.vmemulator.model;


public enum Segment {
    CONSTANT, LOCAL, ARGUMENT, THIS, THAT, TEMP, POINTER, STATIC;
}
