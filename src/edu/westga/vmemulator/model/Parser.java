package edu.westga.vmemulator.model;


import java.io.*;
import java.util.Scanner;

import edu.westga.vmemulator.controller.CodeGen;

public class Parser {

	private Scanner sourceCode;
	private String fileName;
	public Parser(File file){
		this.fileName = file.getName().substring(0,file.getName().length()-3);
		System.out.println("inside constructor:  "+file.getName());
		StringBuilder sb = new StringBuilder();
		try{
			Scanner scan = new Scanner(file);
			while(scan.hasNext()){
				String line = scan.nextLine().trim();
				if(line.length() == 0 || line.contains("//"))
					continue;
				sb.append(line+"\n");
			}
			scan.close();
		}
		catch(IOException err){
			
			System.err.println("IOError " + err);
			System.exit(1);
		}
		sourceCode = new Scanner(sb.toString());
		
	}
	
	public boolean hasNext(){
		return sourceCode.hasNext();
	}
	
	public String parseNext(){
		
		String[] tokens = sourceCode.nextLine().split("\\p{Space}+");
		System.err.println("length: "+tokens.length);
		
		String output="COMMAND NOT FOUND";
		
		if(tokens.length > 1 && tokens[1].equalsIgnoreCase("static")){
			if( tokens[0].equalsIgnoreCase("push")){
				output = CodeGen.static_push(this.fileName, Integer.parseInt(tokens[2]));
			}
			if( tokens[0].equalsIgnoreCase("pop")){
				output = CodeGen.static_pop(this.fileName, Integer.parseInt(tokens[2]));
			}
		}
		else{
			if(tokens[0].equalsIgnoreCase("add")){
				output = CodeGen.add();
			}
			if(tokens[0].equalsIgnoreCase("neg")){
				output = CodeGen.neg();
			}
			if(tokens[0].equalsIgnoreCase("not")){
				output = CodeGen.not();
			}
			if(tokens[0].equalsIgnoreCase("sub")){
				output = CodeGen.sub();
			}
			if(tokens[0].equalsIgnoreCase("eq")){
				output = CodeGen.eq();
			}
			if(tokens[0].equalsIgnoreCase("gt")){
				output = CodeGen.gt();
			}
			if(tokens[0].equalsIgnoreCase("lt")){
				output = CodeGen.lt();
			}
			if(tokens[0].equalsIgnoreCase("or")){
				output = CodeGen.or();
			}
			if(tokens[0].equalsIgnoreCase("and")){
				output = CodeGen.and();
			}
			if(tokens[0].equalsIgnoreCase("push")){
				Segment seg = Segment.valueOf(tokens[1].toUpperCase());
				output = CodeGen.push(seg,Integer.parseInt(tokens[2]));
			}
			if(tokens[0].equalsIgnoreCase("pop")){
				Segment seg = Segment.valueOf(tokens[1].toUpperCase());
				output = CodeGen.pop(seg,Integer.parseInt(tokens[2]));
			}
		}
		return output;
	}
}
