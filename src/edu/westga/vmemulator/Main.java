package edu.westga.vmemulator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.PrintWriter;

import edu.westga.vmemulator.model.Parser;

public class Main {

	public static void main(String[] args) {

		if (args == null || args.length == 0) {
			System.err.println("Need VM code filename");
			System.exit(1);
		}
		String filename = args[0];
		if (filename.endsWith(".vm")) {
			File file = new File(filename);
			translateFile(file);
		} else {
			File dir = new File(filename);
			File[] vmFiles = dir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String filename) {
					return filename.endsWith(".vm");
				}
			});
			
			for(File file : vmFiles){
				translateFile(file);
			}
		}

	}

	private static void translateFile(File file) {
		Parser parser = new Parser(file);
		String output_dir = file.getParent() + "/";
		String output_filename = file.getName().substring(0, (int)file.getName().length() - 2) + "asm";
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(output_dir + output_filename);
		} catch (FileNotFoundException err) {
			System.err.println("Can't create file: " + output_filename);
			System.exit(1);
		}
		while (parser.hasNext())
			writer.println(parser.parseNext());
		writer.close();
	}

}
